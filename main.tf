provider "aws" {
  region = "us-east-1"
}

resource "aws_security_group" "sg" {
  name          = var.name
  vpc_id        = var.vpc_id
  description   = var.description

  tags = {
    Name = "${var.vpc_id}-sg1"
  }

  # Default ingress / egress rules for web servers 
  ingress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  #allow all egress
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}