
#Deploy VPC 
module "vpc"{
    source                                  = "git::https://gitlab.com/ricardocg-tf-aws/tf-vpc.git?ref=master"
    vpc_cidr                                = "10.0.0.0/16"
    env                                     = "prod"
}

module "security_group"{
    source          = "../../../"
    vpc_id          = module.vpc.vpc_id
    name            = "Test-sg"
    description     = "Kitchen test SG module"
}