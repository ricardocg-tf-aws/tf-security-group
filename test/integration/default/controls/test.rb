sg_id = attribute ("security_group_id")

control "sg_test" do
    impact 1

    describe aws_security_group(group_id: sg_id) do
        it { should exist }
    end
end