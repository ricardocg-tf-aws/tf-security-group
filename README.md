# AWS SECURITY GROUP MODULE 

- Deploys Security Groups in AWS for use in VPC. 

# Inputs 

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| VPC | The ID of the VPC | String | - |:yes:|
| Nmae | Name for the SG | String | my-sg |:yes:|
| description | Brief description | List |  |:yes:|

# Outputs 

| Name | Description |
|------|-------------|
| Security group ID | ID of the group created |

# Usage

```js
module "security_group"{
    source          = "../../../"
    vpc_id          = module.vpc.vpc_id
    name            = "Test-sg"
    description     = "Kitchen test SG module"
}
```