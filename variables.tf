variable "name"{
    description = "Name for te SG"
    type = "string"
    default = "my-sg"
}

variable "vpc_id"{
    description = "VPC ID for the network"
    type = "string"
}

variable "description"{
    description = "Description of the SG"
    type = "string"
    default = "my description"
}